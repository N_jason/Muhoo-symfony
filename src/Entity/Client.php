<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface ;
use JsonSerializable ;
use Doctrine\Common\Collections\ArrayCollection;
use App\Repository\TransactionRepository;
/**
 * @ORM\Table(name="app_client")
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client extends User
{
    
  /**
   * nom container
   * @var string
    * @ORM\Column(type="string")
    */
  private $nom;
  /**
  * @ORM\Column(type="string")
  */
  private $prenom;
  /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

  public function getRoles()

  {
     $roles = $this->roles;

     // guarantees that a user always has at least one role for security

     if (empty($roles)) {

         $this->setRoles('ROLE_USER');
         $roles = $this->roles;

     }

     return $this->roles;
  }
  

 
  public function jsonSerialize()
  {
    return array(
        "id"        =>$this->id,
        "username"  =>$this->username,
        "password"  =>$this->password,
        "nom"       =>$this->nom,
        "prenom"    =>$this->prenom,
        "created_at"=>$this->created_at->format('Y-m-d H:i:s')

    );
  }

    /**
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     *
     * @return self
     */
    public function setPrenom($prenom):?Client
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @return string
     */
    public function getNom():?string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     *
     * @return self
     */
    public function setNom($nom):?Client
    {
        $this->nom = $nom;

        return $this;
    }
     public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }
     public function getCreated_at(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreated_at(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at ?? new \DateTime('now');

        return $this;
    }
}

    

    

   