<?php

namespace App\Form;

use App\Entity\Transaction;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;


class ApiTransactionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('client_id')
                ->add('commercant_id')
                ->add('reduction')
                ->add('created_at');
       
    }
    public function getClientId(){
        return "transaction";
    }
    public function getCommercantId(){
        return "transaction";
    }
    public function getReduction(){
        return "transaction";
    }
    public function getCreatedAt(){
        return "transaction";
    }
   
}
