<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use \App\Entity\Client;
use \App\Entity\Commercant;
/**
 * @ORM\Table(name="app_transaction")
 * @ORM\Entity(repositoryClass="App\Repository\TransactionRepository")
 */
class Transaction
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $client;

    /**
     * @ORM\Column(type="integer")
     */
    private $commercant;
    /**
     * @ORM\Column(type="datetime",name="created_at")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $reduction_id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Collection|User[]
     */
    public function getclient()
    {   

        return $this->client;
    }

    public function setclient(Client $client): self
    {
        $this->client = $client->getId();
        return $this;
    
    }
    /**
     * 
     */
    public function getCommercant()
    {
        return $this->commercant;
    }

    

    public function setCommercant(Commercant $commercant): self
    {
       $this->commercant = $commercant->getId();
        return $this;
    }
    public function setCreatedAt()
    {
     $this->createdAt = $this->createdAt  ?? new \DateTime("now");
    }public function getCreatedAt()
    {
     return $this->createdAt;
    }


    public function getReduction():?int
    {
        return $this->reduction_id;
    }
    public function setReduction($reduction)
    {
         $this->reduction_id = $reduction->getId();
    }
   
    public function isValid(){
        return isset($this->reduction_id) && 
        isset($this->createdAt) && 
        isset($this->commercant) &&
        isset($this->client);
    }
    public function jsonSerialize()
    {
        return array(
            "id"=>$this->id,
            "idcommercant"=>$this->commercant,
            "idreduction"=>$this->reduction_id,
            "idclient"=>$this->client,
            "createdAt"=>$this->createdAt->format('Y-m-d H:i:s')

        );
    }
}
