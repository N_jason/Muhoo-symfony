<?php

namespace App\Controller;

use App\Entity\Reduction;
use App\Form\ReductionType;
use App\Repository\ReductionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/reduction")
 */
class ReductionController extends Controller
{
    /**
     * @Route("/", name="reduction_index", methods="GET")
     */
    public function index(ReductionRepository $reductionRepository): Response
    {
        return $this->render('reduction/index.html.twig', ['reductions' => $reductionRepository->findAll()]);
    }

    /**
     * @Route("/new", name="reduction_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $reduction = new Reduction();
        $form = $this->createForm(ReductionType::class, $reduction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($reduction);
            $em->flush();
            // foreach($reduction->getProduits()as $article);{
            //     $article->addReductions($reduction);
            // }

            return $this->redirectToRoute('reduction_index');
        }

        return $this->render('reduction/new.html.twig', [
            'reduction' => $reduction,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="reduction_show", methods="GET")
     */
    public function show(Reduction $reduction): Response
    {
        return $this->render('reduction/show.html.twig', ['reduction' => $reduction]);
    }

    /**
     * @Route("/{id}/edit", name="reduction_edit", methods="GET|POST")
     */
    public function edit(Request $request, Reduction $reduction): Response
    {
        $form = $this->createForm(ReductionType::class, $reduction);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($form->isSubmitted() && $form->isValid()) {
            $articles = $reduction->getProduits()->toArray();
            foreach ($articles as $article){
                $reduction->addProduits($article);

            }
            $em->merge($reduction);
            $em->flush();
            dump($em->getRepository(Reduction::class)->findAll());
            die;
            
            return $this->redirectToRoute('reduction_edit', ['id' => $reduction->getId()]);
        }
        return $this->render('reduction/edit.html.twig', [
            'reduction' => $reduction,
            'articles' => $reduction->getProduits(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="reduction_delete", methods="DELETE")
     */
    public function delete(Request $request, Reduction $reduction): Response
    {
        if ($this->isCsrfTokenValid('delete'.$reduction->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($reduction);
            $em->flush();
        }

        return $this->redirectToRoute('reduction_index');
    }
}
