@ECHO off

IF EXIST var\data.db (
	del .\var\data.db
	)


php bin/console doctrine:schema:create &&
php bin/console doctrine:migrations:generate && php bin/console doctrine:migrations:migrate &&


PAUSE
