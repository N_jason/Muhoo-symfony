<?php

namespace App\Repository;

use App\Entity\Commercant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Commercant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commercant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commercant[]    findAll()
 * @method Commercant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommercantRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Commercant::class);
    }

//    /**
//     * @return User[] Returns an array of User objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username ')
            ->setParameter('username', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }
   /* public function findOneByApiKey($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.apiKey = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }*/
    
    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    
    public function findByCommercant(): ?array
    {
        
        return $this->createQueryBuilder('u')
            ->andWhere('u.roles = :val')
            ->setParameter('val', 'ROLE_COM')
            ->addOrderBy('u.username', 'ASC')
            ->getQuery()
            ->getResult()
        
        ;
    }
    
    
}
