<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\Commercant;
use App\Entity\Client;
use App\Entity\Article;
use App\Entity\Reduction;
class AppFixtures extends Fixture
{
	private $encoder;

	public function __construct(UserPasswordEncoderInterface $encoder)
	{
	    $this->encoder = $encoder;
	}

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        // fixtures USER
        $commercantList = [
        	0=>[
        		"username"=>"intersport",
        		"password"=>"intersport"
        	],
        	1=>[
        		"username"=>"Amazon",
        		"password"=>"Amazon"
        	],
        	2=>[
        		"username"=>"IUT_Orleans",
        		"password"=>"IUTOrleans"
        	],
        	3=>[
        		"username"=>"Gifi",
        		"password"=>"Gifi"
        	],

        ];
        foreach($commercantList as $key=>$user){
	    	$commercant = new Commercant();
	    	$commercant->setUsername($user['username']);
	    	$password = $this->encoder->encodePassword($commercant, $user['password']);
	        $commercant->setPassword($password);
	        $commercant->setIsActive(true);
	        $commercant->setRoles();
	        $manager->persist($commercant);
        }
        $clientList = [
        	0=>[
				"username"   =>"jneves",
				"password"   =>"jason",
				"nom"        =>"Neves",
				"prenom"     =>"Jason",
				"created_at" =>	new \Datetime('now')
        	],
        	1=>[
				"username"   =>"Fmartin",
				"password"   =>"florian",
				"nom"        =>"Martin",
				"prenom"     =>"Florian",
				"created_at" =>	new \Datetime('now')
        	],
        	2=>[
				"username"   =>"client",
				"password"   =>"client",
				"nom"        =>"nomClient",
				"prenom"     =>"prenomClient",
				"created_at" =>	new \Datetime('now')
        	],
        ];
        foreach($clientList as $key=>$user){
	    	$client = new Client();
	    	$client->setUsername($user['username']);
	    	$password = $this->encoder->encodePassword($client, $user['password']);
	        $client->setPassword($password);
	        $client->setIsActive(true);
	        $client->setCreated_at($user['created_at']);
	        $client->setNom($user['nom']);
	        $client->setPrenom($user['prenom']);

	       	$manager->persist($client);

        }
        $manager->flush();
        // fixtures Article

        $articleList =  [
			1 => [
				'nom'         => "ballon",
				'note'        => '1.2',
				'description' => 'Ballon de chez intersport',
				'prix'        => 10,
				'commercant'  => "intersport"],
			2 => [
				'nom'         => "Enceinte bluetooth",		
				'note'        => '5',
				'description' => 'Enceinte bluetooth nommade',
				'prix'        => 20,
				'commercant'  => "Amazon"],
			3 => [
				'nom'         => "Enceinte bluetooth sport",		
				'note'        => '1.2',
				'description' => 'Enceinte bluetooth ultra résistante',
				'prix'        => 19.99,
				'commercant'  => "intersport"],
			4 => [
				'nom'         => "Pull",		
				'note'        => '10',
				'description' => "pull de l'IUT",
				'prix'        => 100,
				'commercant'  => "IUT_Orleans"],
			5 => [
				'nom'         => "Lampe de poche",		
				'note'        => '20',
				'description' => 'Lampe de poche puissante',
				'prix'        => 16.3,
				'commercant'  => "intersport"],
			6 => [
				'nom'         => "Lampe de chevet",		
				'note'        => '1.2',
				'description' => 'Lampe de chevet',
				'prix'        => 19.99,
				'commercant'  => "intersport"],
			7 => [
				'nom'         => "lampe de bureau",		
				'note'        => '10',
				'description' => "lampe de bureau",
				'prix'        => 100,
				'commercant'  => "Gifi"],
			8 => [
				'nom'         => "lampe bluetooth",		
				'note'        => '20',
				'description' => 'lampe bluetooth 2.0',
				'prix'        => 20.0,
				'commercant'  => "Amazon"],
			9 => [
				'nom'         => "Lampe de poche",		
				'note'        => '5',
				'description' => 'Lampe de poche Gifi',
				'prix'        => 20.0,
				'commercant'  => "Gifi"],

			];

        foreach($articleList as $key=>$value){
        	$article=new Article();
        	foreach($value as $name=>$val)
			{
        	$article->set($name,$val);
        	
			}
			$article_com = $manager->getRepository(\App\Entity\Commercant::class)->loadUserByUsername($value['commercant']);
			
        	$article->setCommercant($article_com);
			$manager->persist($article);
		}
        $manager->flush();

        $deleted_at = 	new \Datetime('now');
        $deleted_at->modify('+1 day');
        $reductionList=[
        	0 => [
				"produits"   =>	["article_1"=>2,"article_2"=>3],
				"libelle"    =>	"enceinte d'été",
				"cout"       =>	10,
				"stock"      =>	100,
				"created_at" =>	new \Datetime('now'),
				"deleted_at" => $deleted_at
        	],
        	1 => [
				"produits"   =>	["article_1"=>1,"article_2"=>5],
				"libelle"    =>	"Reduction sport",
				"cout"       =>	10,
				"stock"      =>	100,
				"created_at" =>	new \Datetime('now'),
				"deleted_at" => $deleted_at
        	],
        	2 => [
				"produits"   =>	["article_1"=>6,"article_2"=>7,"article_3"=>8,"article_4"=>9],
				"libelle"    =>	"Reduction Lampe",
				"cout"       =>	10,
				"stock"      =>	100,
				"created_at" =>	new \Datetime('now'),
				"deleted_at" => $deleted_at
        	],
        	3 => [
				"produits"   =>	["article_1"=>4],
				"libelle"    =>	"Reduction textile",
				"cout"       =>	10,
				"stock"      =>	100,
				"created_at" =>	new \Datetime('now'),
				"deleted_at" => $deleted_at
        	],
        	4 => [
				"produits"   =>	["article_1"=>1,"article_2"=>2,"article_3"=>3,"article_4"=>4],
				"libelle"    =>	"Reduction premium",
				"cout"       =>	10,
				"stock"      =>	100,
				"created_at" =>	new \Datetime('now'),
				"deleted_at" => $deleted_at
        	]
        ];
        $articleRepository =  $manager->getRepository(\App\Entity\Article::class);
        foreach($reductionList as $key=>$value){
        	
        	$reduction = new Reduction();
			foreach( $value["produits"] as $p=> $prod_id)     {
				$reduction->addProduits($articleRepository->findOneById($prod_id));
			}
			$reduction->setLibelle( $value["libelle"] ) ;
			$reduction->setCout( $value["cout"] ) ;       
			$reduction->setStock( $value["stock"] ) ;
			$reduction->setCreated_at( $value["created_at"] ) ;
			
			$reduction->setDeleted_at( $value["deleted_at"] ) ;
			$manager->persist($reduction);
		}
		$manager->flush();
    }
}
