<?php

namespace App\Form;

use App\Entity\Transaction;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;


class TransactionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('client')
            //->add('commercant')
            ->add('createdAt', DateTimeType::class, array( 'widget' => 'choice','data'=> new \Datetime('now')))
        ;
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event)  {
                $form = $event->getForm();

                $formOptions =array(
                    'class' => \App\Entity\Commercant::class,
                    'choice_label' => 'username',               
                );
                $form->add('commercant', EntityType::class, $formOptions);
            }
        );
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event)  {
                $form = $event->getForm();

                $formOptions =array(
                    'class' => \App\Entity\Client::class,
                    'choice_label' => 'username',               
                );
                $form->add('client', EntityType::class, $formOptions);
            }
        );
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event)  {
                $form = $event->getForm();

                $formOptions =array(
                    'class' => \App\Entity\Reduction::class,
                    'choice_label' => 'libelle',               
                );
                $form->add('reduction', EntityType::class, $formOptions);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Transaction::class,
        ]);
    }
}
