<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable ;
use \Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Entity\Reduction;
use App\Entity\Commercant;

/**
 * @ORM\Table(name="app_Article")
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 */
class Article implements JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="integer")
     */
    private $note;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prix;

    /**
     * @ORM\ManyToOne(targetEntity="Commercant")
     *
     */
    private $commercant;

    /**
     * @ORM\ManyToMany(targetEntity="Reduction",inversedBy="produits")
     * @ORM\JoinTable(name="article_reduction")
     */
    private $reductions ;


    public function __construct() {
        $this->reductions = new ArrayCollection();
    }
    public function getId()
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }
    public function set(string $name,string $value)     {

        if ($name !="commercant")
        return $this->{'set'.ucfirst($name)}($value);
    return $this;


    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(?float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }
    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?String $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCommercant(): ?Commercant
    {
        return $this->commercant;
    }

    public function setCommercant(Commercant $commercant): self
    {
        $this->commercant = $commercant;

        return $this;
    }
    public function jsonSerialize()
    {
        return array(
            'id'=>$this->id,
            'nom'=>$this->nom,
            'note'=>$this->note,
            'prix'=>$this->prix,
            'description'=>$this->description,
            'commercant'=>$this->commercant->getId(),
        );
    }
     public function addreductions(Reduction $reduction)
    {
         if ($this->reductions->contains($reduction)) {
            return null;
        }
        $this->reductions->add($reduction);
        dump($this->reductions);
        $reduction->addProduits($this);
        return $this->reductions;
    }
    public function removereductions(Reduction $reduction)
    {
        if (!$this->reductions->contains($reduction)) {
            return null;
        }
        $this->reductions->removeElement($reduction);
        $reduction->removeProduits($this);
    }

    public function getReductions()
    {
        return $this->reductions;
    }
    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->nom,
            $this->note,
            $this->prix,
            $this->commercant
        ));
    }

    public function unserialize($serialized)
    {
        list (
             $this->id,
            $this->nom,
            $this->note,
            $this->prix,
            $this->commercant
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }
}
