#!/bin/bash
echo $1
if [ -n "$1" ] && [ $1 == 'drop' ];
then
    rm -f ./var/data.db
	rm -f ./src/Migrations/Version*.php
	php bin/console doctrine:schema:create && php bin/console doctrine:migrations:generate -n
	 php bin/console doctrine:fixtures:load
else
  php bin/console doctrine:migrations:diff
fi



 php bin/console doctrine:migrations:migrate -n
