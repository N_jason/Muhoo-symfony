# Installation

## Pré-requis

!!! Attention nous utilisons symfony4 avec la dernière version de PHP 7.2.4 -> il faut donc impérativement installer ces versions avant de commencer !!!

## Première partie
	> composer install
	> bash drop_generate_db.sh

## Deuxième partie : installation du JWT
	> mkdir config/jwt
    > openssl genrsa -out config/jwt/private.pem
    > openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem



## Server
	 Une fois les commandes réaliser ci-dessus, vous avez à présent des données dans votre base et ainsi lancer votre serveur avec la commande suivante :

	 	> php bin/console s:r 0.0.0.0:8080

## Android

	Une fois le serveur démarré, aller voir le readme du projet Android




# Annexe

## Pour le server
	GET /Commercant  : (index,show,new,edit)
	GET /Client      : (index,show,new,edit)
	GET /Transaction : (index,show,new,edit)
	GET /Reduction   : (index,show,new,edit)

## Pour l'app cliente

	les routes chez le client sont accessiblent par un utilisateur enregistré en tant que Commercant.

## Pour la connection


	POST /api/login_check => authentification par json sous le format

		{
		    "security": {
		        "credentials": {
		            "username": "{{username}}",
		            "password": "{{password}}"
		        }
		    }
		}


![login check](./img/post_login_check.png "Login")

	En réponse on récupère un token sinon une erreur 401 est retourné

![validation du login](./img/post_login_check_response.png "Login valider")
![exampleauth](./img/post_login_check_bad_response.png "Login erroné")


### Les autres routes

	Information importante pour garder un maximum de sécurité.
	Dans le header de chaque requête il faut envoyer le token sous le format :
		"Autorization=> Bearer {{token}}

![headers avec le token](./img/headers.png "headers")


	GET /api/           => {user:{{info_user}}}
	GET /api/commercant => {
							"commercants":
									{liste[{"id":{commercant_id},"username":{commercant_username}}]}
						} retourne la liste des commercants
	GET /api/client     => {
								"clients":
										{liste[{"id":{client_id},"username":{client_username},'date_transaction':{{date_dernière_transaction ou null}}]}
							}
	GET /api/client/{id} => {'client'=>{"id"=>{client_id},"username"=>{client_username},'date_transaction':{{date_dernière_transaction ou null}}}

	GET /api/commercant/{id} =>{reductions:{'article':{information sur l'article},'reduction':{information sur la réduction}}}

	GET /api/commercant/{id}/articles => retourne les articles du commercant id

	POST /api/transaction =>
						{
							"transaction": {
								"client_id": {{client_id}},
								"commercant_id": {{commercant_id}},
								"reduction": {{reduction_id}}
							}
						}
						en retour on récupère la transaction avec la date de création mise à jours
![Une transaction](./img/post_transaction.png "transaction")
![reponse de la transaction](./img/post_transaction_response.png "transaction reponse")
