<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface ;
use JsonSerializable ;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="app_commercant")
 * @ORM\Entity(repositoryClass="App\Repository\CommercantRepository")
 */
class Commercant extends \App\Entity\User 
{
   
    
    public function getRoles()
 
    {
      
       $roles = $this->roles;
 
       // guarantees that a user always has at least one role for security
 
       if (empty($roles)) {
 
           $this->setRoles('ROLE_COM');
           $roles = $this->roles;
 
       }
 
       return array_unique([$this->roles]);
    }
    public function setRoles($roles = 'ROLE_COM'){
      $this->roles = $roles;
    }
   
    public function jsonSerialize()
    {
        return array(
            $this->id,
            $this->username,
            $this->password,

        );
    }
    
}
