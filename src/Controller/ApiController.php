<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;
use App\Repository\CompteRepository;
use App\Repository\ReductionRepository;
use App\Repository\CommercantRepository;
use App\Repository\ClientRepository;
use App\Repository\TransactionRepository;
use App\Entity\Article;
use App\Entity\Reduction;
use App\Entity\Commercant;
use App\Entity\Client;
use App\Entity\Transaction;
use App\Form\ApiTransactionType;
/**
     * @Route("/api", name="article")
     */
class ApiController extends Controller
{
    /**
     * @Route("/", name="index_api", methods="GET")
     */
    public function index(Request $request)
    {
        //$this->get('lexik_jwt_authentication.jwt_manager')->setUserIdentityField(null);
        // dump($this->get('session'));
        return  new JsonResponse(['user'=>$this->getUser(),'session'=>$request->getContent()]);
    }


    /**
     * @Route("/commercant",name="session_commercant", methods="GET")
     *
     */
    public function commercantAction(Request $request,CommercantRepository $userRepository){

        $users = $userRepository->findAll();
        $callback = function($u) {return ["id"=>$u->getId(),"username"=>$u->getUsername()]; };
        return new JsonResponse( ['commercants'=>array_map($callback,$users)]);


    }/**
     * @Route("/client/{client}",name="api_client_info", methods="GET",defaults={"client" = null})
     *
     */
    public function clientAction(Request $request,ClientRepository $userRepository,TransactionRepository $transactionRepository,Client $client=null)
    {
        if (!$client){
            $users = $userRepository->findAll();
             $callback = function($u) use ($transactionRepository) {
                $transaction = $transactionRepository->findOneBy(['client'=>$u->getId()],["id"=>"DESC"]);

                return ["id"=>$u->getId(),
                "username"=>$u->getUsername(),
                'date_transaction'=>$transaction?$transaction->getCreatedAt():null];
            };


            return new JsonResponse( ['clients'=>array_map($callback,$users)]);
        }

        $date_transaction = $transactionRepository->findByClient($client);
        if(count($date_transaction)!=0){
            $nb_transaction   = count($date_transaction);
            $date_transaction = end($date_transaction)->jsonSerialize()["createdAt"];


        }
        else{
            $date_transaction = null;
            $nb_transaction   = 0 ;
        }
        $json                     = $client->jsonSerialize();
        $json["date_transaction"] = $date_transaction;
        $json['nb_transaction']   = $nb_transaction;
           return new JsonResponse(['client'=>$json]);
    }




    /**
     * @Route("/commercant/{id}",name="api_commercant_id",methods="GET")
     * @ParamConverter("User", class="App\Entity\Commercant")
     */
    public function commercantShowAction(Request $request,Commercant $user,ArticleRepository $articleRepository,ReductionRepository $reductionRepository){
        $articles = $articleRepository->findBy(["commercant"=>$user->getId()]);
        return   new JsonResponse(

                [
                    'reductions'=>$reductionRepository->findReductionByCommercantArticles($articles)
                ]

        );


    }
    /**
     * @Route("/commercant/{id}/articles",name="api_commercant_article",methods="GET")
     * @return all Article
     */
    public function articlesCommercantAction( ArticleRepository $articleRepository,Commercant $commercant): JsonResponse
    {

        //$this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return new JsonResponse( ['articles' =>$articleRepository->findBy(["commercant"=>$commercant->getId()])]);
    }
     /**
     * @Route("/commercant/{id}/reductions",name="api_commercant_reduction",methods="GET")
     * @return all Article
     */


    /**
     * @Route("/transaction",name="api_transaction",methods="POST")
     * @return all Reduction
     */
    public function transactionAction(Request $request,ReductionRepository $reductionRepository,ClientRepository $clientRepository,CommercantRepository $commercantRepository ): JsonResponse
    {

        //$this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $transaction = new Transaction();
        //dump($form->all());
        $transaction_content = json_decode($request->getContent());
        $transaction_content = $transaction_content->transaction;
        // récupération du contenue
        $reduc_id      = $transaction_content->reduction;
        $client_id     = $transaction_content->client_id;
        $commercant_id = $transaction_content->commercant_id;
        // validation du contenue
        $reduction  = $reductionRepository->findOneBy(['id'=>$reduc_id]);
        $client     = $clientRepository->find($client_id);
        $commercant = $commercantRepository->find($commercant_id);


        //enregistrement
        if ($reduction && $client && $commercant){
            $transaction->setclient($client);
            $transaction->setCommercant($commercant);
            $transaction->setCreatedAt();
            $transaction->setReduction($reduction);

        }

        if ($transaction->isValid()) {
            $reduction->setMinusOneStock();
            $em = $this->getDoctrine()->getManager();
            $em->persist($reduction);
            $em->persist($transaction);
            $em->flush();

            return  new JsonResponse( ['rep' => "good value","transaction"=>$transaction->jsonSerialize()]);
        }
        return new JsonResponse( ['rep' => 'bad value']);
    }





}
