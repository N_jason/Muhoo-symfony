<?php

namespace App\Form;

use App\Entity\Reduction;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;


class ReductionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('libelle')
        ->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event)  {
                $form = $event->getForm();

                $formOptions =array(
                'class' => \App\Entity\Article::class,
                'choice_label' => 'nom',
                'multiple' => true,      
                );
                $form->add('produits', EntityType::class, $formOptions); 
                 }
        );
        $builder
            ->add('cout',null,array('label' => 'Réduction (%)'))
            ->add('stock', null)
            ->add('created_at', DateType::class, array( 'widget' => 'choice','data'=> new \Datetime('now')))
            ->add('deleted_at', DateType::class, array( 'widget' => 'choice','data'=> new \Datetime('now')));
            // ->add('cout')
            // ->add('created_at')
            // ->add('deleted_at')
            // ->add('produits')
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reduction::class,
        ]);
    }
}
