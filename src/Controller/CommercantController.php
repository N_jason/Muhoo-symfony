<?php

namespace App\Controller;

use App\Entity\Commercant;
use App\Form\CommercantType;
use App\Repository\CommercantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/commercant")
 */
class CommercantController extends Controller
{
    /**
     * @Route("/", name="commercant_index", methods="GET")
     */
    public function index(CommercantRepository $commercantRepository): Response
    {
        return $this->render('commercant/index.html.twig', ['commercants' => $commercantRepository->findAll()]);
    }

    /**
     * @Route("/new", name="commercant_new", methods="GET|POST")
     */
    public function new(Request $request,UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $commercant = new Commercant();
        $form = $this->createForm(CommercantType::class, $commercant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($commercant, $commercant->getPassword());
            $commercant->setPassword($password);
            $commercant->setRoles();
            dump($commercant);
            $em = $this->getDoctrine()->getManager();
            $em->persist($commercant);
            $em->flush();

            return $this->redirectToRoute('commercant_index');
        }

        return $this->render('commercant/new.html.twig', [
            'commercant' => $commercant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="commercant_show", methods="GET")
     */
    public function show(Commercant $commercant): Response
    {
        return $this->render('commercant/show.html.twig', ['commercant' => $commercant]);
    }

    /**
     * @Route("/{id}/edit", name="commercant_edit", methods="GET|POST")
     */
    public function edit(Request $request, Commercant $commercant): Response
    {
        $form = $this->createForm(CommercantType::class, $commercant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('commercant_edit', ['id' => $commercant->getId()]);
        }

        return $this->render('commercant/edit.html.twig', [
            'commercant' => $commercant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="commercant_delete", methods="DELETE")
     */
    public function delete(Request $request, Commercant $commercant): Response
    {
        if ($this->isCsrfTokenValid('delete'.$commercant->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($commercant);
            $em->flush();
        }

        return $this->redirectToRoute('commercant_index');
    }
}
