<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface ;
use JsonSerializable ;
use Doctrine\Common\Collections\ArrayCollection;
//iniritence type
/**
 *@ORM\MappedSuperclass
 * 
 */
abstract class User implements UserInterface, \Serializable,JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255,unique=True)
     */
    protected $username;

    /**
     * @ORM\Column(type="string")
     */
    protected $password;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive;

   
    /**
     * @ORM\Column(name="roles", type="string", length=20)
     */
    protected $roles;

    

    public function __construct($username =null, $password=null, $salt=null,$apiKey=null)
    {
        $this->username = $username;
        $this->password = $password;
        //$this->salt = $salt;
        $this->roles = $this->getRoles();
        $this->isActive = true;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getRoles() 
    {
       $roles = $this->roles;
 
       // guarantees that a user always has at least one role for security
 
       if (empty($roles)) {
 
           $this->setRoles(array('ROLE_USER'));
           $roles = $this->roles;
 
       }
 
       return array_unique($roles);
    }

    public function setRoles($roles)
    {
       $this->roles = $roles;

    }
    /*public function isRoles(String $role): ?boolean
    {
       return $this->roles.contains($role);

    }*/

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,

        ));
    }
    public function jsonSerialize()
    {
        return array(
            $this->id,
            $this->username,
            $this->password,

        );
    }
     public function eraseCredentials()
    {
    }
    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }
    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password): self
    {
        $this->password = $password;

        return $this;
    }
   
    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }
    public function logout(){}

    

   
}
