<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeFileSessionHandler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
//use App\Repository\UserRepository;
use App\Repository\ClientRepository;
use App\Repository\CommercantRepository;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;


use GuzzleHttp\Client;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
/**
 * @Route("/")
 */
class SessionController extends Controller
{
    /**
     * @Route("/session", name="session", methods="GET")
     */
    public function index()
    {
        return $this->render('session/index.html.twig',["controller_name"=>"session"]);
    }
    /**
     * @Route("/login",name="session_login")
     * 
     */
    public function loginAction(Request $request,ClientRepository $clientRepository,CommercantRepository $commercantRepository,UserPasswordEncoderInterface $encoder){
      // Ancien moyen de connection 
  		$session = $request->getSession();
    	$username = $request->query->get('username');
    	$password = $request->query->get('password');
    	$type = $request->query->get('type');
  		// $em = $this->getDoctrine()->getManager();
  		
    	//TODO : condition celon le role
    	if ($type === "client"){
	  		$user = $clientRepository->findOneBy(['username'=>$username]);
    		
    	}elseif ($type === "commercant") {
             $user = $commercantRepository->findOneBy(['username'=>$username]);
    		
    	}
  		

		// Vérification
        // Check if the user exists !
        if(!$user){
            return new JsonResponse(
                'Username doesnt exists',
                Response::HTTP_UNAUTHORIZED,
                array('Content-type' => 'application/json')
            );
        }

        $session->set('uid',$user->getId());

        $salt = $user->getSalt();
		if(!$encoder->isPasswordValid($user, $password)) {
           return new JsonResponse(
                'Username or Password not valid.',
                Response::HTTP_UNAUTHORIZED,
                array('Content-type' => 'application/json')
            );
          
        } 
        $token = new UsernamePasswordToken($user, null, 'api', [$user->getRoles()]);
        $this->get('security.token_storage')->setToken($token);
        $user->setApiKey($token);
        $this->get('session')->set('_security_api', serialize($token));
        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

  		return new JsonResponse(
  				array('token'=>$token->serialize(),'username'=>$username,"encoded"=>$encoder->encodePassword($user,$password),'pswd'=>$user->getPassword(),'user'=>$this->getUser())

  		);

    }

    /**
     * @Route("/logout",name="session_logout")
     * 
     */
    public function logoutAction(Request $request){
		$session = $request->getSession();
		$session->clear();
    	return new JsonResponse(array('token' => $session->get("token"),'attributes'=>$session->getMetadataBag(),'user'=>$this->getUser()));


    }
    /**
     * @Route("/profil",name="session_profil")
     * 
     */
    public function profileAction(Request $request){
		$session = $request->getSession();
		dump($this->get('security.csrf.token_manager'));
		
    	return new JsonResponse(array('attributes'=>$session->get('uid')));


    }
}
