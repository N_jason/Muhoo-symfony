<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
 /**
 * @Route("/api", name="")
 */
class IndexController extends Controller
{
    /**
     * @Route("/", name="index_controller")
     */
    public function indexAction(UserInterface $user = null)
	{
		return $this->render('index_controller/index.html.twig', [
	            'controller_name' => 'IndexController',
	        ]);
	}

    /**
     * @Route("/admin")
     */
    public function admin()
    {
        return new Response('<html><body>Admin page!</body></html>');
    }
    /**
     * @Route("/post",name="post")
     */
    public function Post()
    {
        
        return new Response('<html><body>Post page!</body></html>');
    }
    
}
