<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable ;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Entity\Article;

/**
 * @ORM\Table(name="app_reduction")
 * @ORM\Entity(repositoryClass="App\Repository\ReductionRepository")
 */
class Reduction implements JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="Article", mappedBy="reductions")
     *
     */
    private $produits;


    /**
     * @ORM\Column(type="string",length=255)
     */
    private $libelle;
    /**
     * @ORM\Column(type="float")
     */
    private $cout;

    /**
     * @ORM\Column(type="integer")
     */
    private $stock;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    public function __construct() {
        $this->produits = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }
     public function getLibelle()
    {
        return $this->libelle;
    }
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
        return $this;
    }

    public function addProduits(Article $produit)
    {
         if ($this->produits->contains($produit)) {
            return null;
        }
        $this->produits->add($produit);
        $produit->addReductions($this);
        return $this->produits;
    }
    public function removeProduits(Article $produit)
    {
        if (!$this->produits->contains($produit)) {
            return;
        }
        $this->produits->removeElement($produit);
        $produit->removeReductions($this);
    }

     /**
     *
     */
    public function getProduits(): ?Collection
    {
        return $this->produits;
    }


    public function getCout(): ?float
    {
        return $this->cout;
    }

    public function setCout(float $cout): self
    {
        $this->cout = $cout;

        return $this;
    }
    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }
     public function setMinusOneStock(): self
    {
        $this->stock -=1;

        return $this;
    }
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setDeletedAt(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }
    public function getcreated_at(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setcreated_at(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getdeleted_at(): ?\DateTimeInterface
    {
        return $this->deleted_at;
    }

    public function setdeleted_at(?\DateTimeInterface $deleted_at): self
    {
        $this->deleted_at = $deleted_at;

        return $this;
    }
     public function jsonSerialize()
    {
        $produits = [];
        foreach($this->produits as $p){
            array_push($produits,$p->jsonSerialize());
        }
        return array(
            'id'         =>$this->id,
            'libelle'    =>$this->libelle,
            'cout'       =>$this->cout,
            'stock'      =>$this->stock,
            'created_at' =>$this->created_at,
            'deleted_at' =>$this->deleted_at,
            'produits'   =>$produits
            
        );
    }
    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->produit_id,
            $this->cout,
            $this->stock,
            $this->created_at,
            $this->deleted_at
        ));
    }
    public function unserialize($serialized)
    {
        list (
             $this->id,
            $this->produit_id,
            $this->cout,
            $this->stock,
            $this->created_at,
            $this->deleted_at
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }
    public function __toString() {
        return $this->libelle;
    }
}
